package com.hse.metrology.mikhailov.service;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Service
public class ParserService {
    public double[] parseTxt(InputStream stream) throws IOException {
        String text = IOUtils.toString(stream, StandardCharsets.UTF_8);
        text = text.replaceAll(",",".");
        String[] temp = text.split("\t");
        double[] result = Arrays.stream(temp).mapToDouble(Double::valueOf).toArray();
        return result;
    }
}
