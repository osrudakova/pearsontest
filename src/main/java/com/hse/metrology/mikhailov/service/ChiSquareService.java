package com.hse.metrology.mikhailov.service;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.exception.*;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.util.FastMath;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Arrays;

@Service
public class ChiSquareService implements Serializable {

    private double chiSquare(final double[] expected, final double[] observed)
            throws NotPositiveException, NotStrictlyPositiveException,
            DimensionMismatchException {

        double sumExpected = 0d;
        double sumObserved = 0d;
        for (int i = 0; i < observed.length; i++) {
            sumExpected += expected[i];
            sumObserved += observed[i];
        }
        double ratio = 1.0d;
        boolean rescale = false;
        if (FastMath.abs(sumExpected - sumObserved) > 10E-6) {
            ratio = sumObserved / sumExpected;
            rescale = true;
        }
        double sumSq = 0.0d;
        for (int i = 0; i < observed.length; i++) {
            if (rescale) {
                final double dev = observed[i] - ratio * expected[i];
                sumSq += dev * dev / (ratio * expected[i]);
            } else {
                final double dev = observed[i] - expected[i];
                sumSq += dev * dev / expected[i];
            }
        }
        return sumSq;
    }

    private double chiProbability(final double[] expected, final double[] observed)
            throws NotPositiveException, NotStrictlyPositiveException,
            DimensionMismatchException, MaxCountExceededException {

        // pass a null rng to avoid unneeded overhead as we will not sample from this distribution
        final ChiSquaredDistribution distribution =
                new ChiSquaredDistribution(null, expected.length - 1.0);
        double chiSquare = chiSquare(expected, observed);
        return 1.0 - (Double.isInfinite(chiSquare) ? 0.0 : distribution.cumulativeProbability(chiSquare));
    }

    public double isNormalDistribution(final double[] observed,
                           final double alpha, final int numOfTests)
            throws NotPositiveException, NotStrictlyPositiveException,
            DimensionMismatchException, OutOfRangeException, MaxCountExceededException {

        double mean = new Mean().evaluate(observed);
        double sd = new StandardDeviation().evaluate(observed);
        NormalDistribution expected = new NormalDistribution(mean, sd);
        int positiveTest = 0;

        for (int i = 0; i < numOfTests; i++) {
            double result = checkFreqances(expected.sample(observed.length), observed, sd);
            if (!(result < alpha)) {
                positiveTest++;
            }
        }
        return (double) positiveTest / numOfTests;
    }

    private double checkFreqances(double[] expecteData, double[] observedData, double sd) {
        double min = Arrays.stream(expecteData).min().getAsDouble();
        double max = Arrays.stream(expecteData).max().getAsDouble();
        int size = (int) Math.ceil(2  * (max - min) / sd);
        double[] expected = new double[size];
        double[] observed = new double[size];
        int pos;
        for (int i = 0; i < expecteData.length; i++) {
            pos = (int) (2 * (expecteData[i] - min) / sd);
            expected[pos]++;
            pos = (int) (2 * (observedData[i] - min) / sd);
            if (pos < 0) {
                pos = 0;
            } else if (pos > size - 1) {
                pos = size - 1;
            }
            observed[pos]++;
        }
        return chiProbability(expected, observed);
    }

}
