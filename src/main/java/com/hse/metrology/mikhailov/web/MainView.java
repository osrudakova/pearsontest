package com.hse.metrology.mikhailov.web;

import com.hse.metrology.mikhailov.service.ChiSquareService;
import com.hse.metrology.mikhailov.service.ParserService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HtmlComponent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;

/**
 * A sample Vaadin view class.
 * <p>
 * To implement a Vaadin view just extend any Vaadin component and
 * use @Route annotation to announce it in a URL as a Spring managed
 * bean.
 * Use the @PWA annotation make the application installable on phones,
 * tablets and some desktop browsers.
 * <p>
 * A new instance of this class is created for every new user and every
 * browser tab/window.
 */
@Route( value = "pearson")
public class MainView extends VerticalLayout {

    @Autowired
    private ChiSquareService service;

    @Autowired
    private ParserService parserService;

    /**
     * Construct a new Vaadin view.
     * <p>
     * Build the initial UI state for the user accessing the application.
     *
     *
     */
    public MainView() {

        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        Div output = new Div();

        upload.addSucceededListener(event -> {
            Component component = checkFile(event.getMIMEType(), buffer.getInputStream());
            showOutput(event.getFileName(), component, output);
        });

        NumberField alphaField = new NumberField("Alpha");
        alphaField.setMin(0d);
        alphaField.setMax(0.5d);

        IntegerField numberOfTestsField = new IntegerField("Number of tests");
        numberOfTestsField.setMin(1);

        Button submitButton = new Button("Submit");
        submitButton.addClickListener(e->{
            try {
                double result = service.isNormalDistribution(parserService
                                .parseTxt(buffer.getInputStream()), alphaField.getValue(),
                                    numberOfTestsField.getValue()) * 100;
                showOutput("This is normal distribution with " + result + "% credibility", new Div(), output);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        add(upload, alphaField, numberOfTestsField, submitButton, output);
    }

    private Component checkFile(String mimeType, InputStream stream) {
        Div content = new Div();
        String text;
        if (mimeType.startsWith("text")) {
            text = "Uploaded successfully";
        } else {
            text = "ERROR";
        }
        content.setText(text);
        return content;

    }

    private void showOutput(String text, Component content,
                            HasComponents outputContainer) {

        HtmlComponent p = new HtmlComponent(Tag.P);
        p.getElement().setText(text);
        outputContainer.add(p);
        outputContainer.add(content);
    }

}
