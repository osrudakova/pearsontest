package com.hse.metrology.mikhailov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PearsonTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(PearsonTestApplication.class);
    }
}
